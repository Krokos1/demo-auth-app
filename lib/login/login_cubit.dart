import 'dart:convert';

import 'package:auth/auth_info/auth_data.dart';
import 'package:auth/login/login_state.dart';
import 'package:auth/route_names.dart';
import 'package:auth/router.dart';
import 'package:auth/widgets/result_page.dart';
import 'package:collection/collection.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginCubit extends Cubit<LoginState> {
  final formKey = GlobalKey<FormState>();

  LoginCubit() : super(const LoginState());

  void onRegisterButtonClicked() {
    navigatorKey.currentState?.pushNamed(registrationPage);
  }

  String? validateField(String? text) {
    if (text == null || text.isEmpty) {
      return "This field can't be empty";
    }
    return null;
  }

  onUsernameSave(String? username) {
    emit(state.changeState(username: username));
  }

  onPasswordSave(String? password) {
    emit(state.changeState(password: password));
  }

  Future<void> login() async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      final prefs = await SharedPreferences.getInstance();
      final storedAuthData = prefs.getStringList('authData');
      final List<AuthData> authData = storedAuthData == null
          ? []
          : storedAuthData
              .map((e) => AuthData.fromJson(json.decode(e)))
              .toList();
      if (authData.firstWhereOrNull((element) => (element.username ==
                  state.username &&
              element.encryptedPassword ==
                  sha256.convert(utf8.encode(state.password)).toString())) !=
          null) {
        navigatorKey.currentState?.pushNamed(resultPage,
            arguments: const ResultScreen(
              text: 'Success!',
              assetUrl: 'assets/yes.png',
            ));
        return;
      }
      navigatorKey.currentState?.pushNamed(resultPage,
          arguments: const ResultScreen(
            text: "Try again :(",
            assetUrl: 'assets/no.png',
          ));
    }
  }

  @override
  Future<void> close() {
    return super.close();
  }
}
