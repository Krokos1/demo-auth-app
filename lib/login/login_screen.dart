import 'package:auth/login/login_cubit.dart';
import 'package:auth/login/login_state.dart';
import 'package:auth/theme.dart';
import 'package:auth/widgets/default_screen_body_view.dart';
import 'package:auth/widgets/form_button.dart';
import 'package:auth/widgets/form_view.dart';
import 'package:auth/widgets/input_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      BlocProvider(create: (_) => LoginCubit(), child: const LoginScreenView());
}

class LoginScreenView extends StatelessWidget {
  const LoginScreenView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      builder: (context, state) {
        final _loginCubit = context.watch<LoginCubit>();
        return Scaffold(
          backgroundColor: mainColorTheme.background,
          appBar: AppBar(
            elevation: 0,
            title: Align(
                alignment: Alignment.centerRight,
                child: TextButton(
                  onPressed: _loginCubit.onRegisterButtonClicked,
                  child: Text(
                    'Register',
                    style: subTextStyle,
                  ),
                )),
          ),
          body: DefaultScreenBodyView(
            textWidget: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Welcome to Demo Auth App',
                      style: mainTextStyle,
                    ),
                    Text(
                      'You can register and sign in',
                      style: subTextStyle,
                    ),
                  ],
                ),
              ),
            ),
            mainWidget: FormView(
              formKey: _loginCubit.formKey,
              children: [
                InputView(
                  onSaved: _loginCubit.onUsernameSave,
                  validator: _loginCubit.validateField,
                  labelText: 'Username',
                ),
                InputView(
                  onSaved: _loginCubit.onPasswordSave,
                  validator: _loginCubit.validateField,
                  bottomPadding: 50,
                  obscureText: true,
                  labelText: 'Password',
                ),
                FormButton(
                  onPressed: _loginCubit.login,
                  buttonText: 'Sign in',
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
