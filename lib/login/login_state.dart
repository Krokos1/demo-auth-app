import 'package:auth/base/base_state.dart';

class LoginState extends BaseFormState {
  const LoginState({
    String? username,
    String? password,
  }) : super(username: username, password: password);

  LoginState changeState({
    String? username,
    String? password,
  }) =>
      LoginState(
        username: username ?? this.username,
        password: password ?? this.password,
      );
}
