import 'package:auth/login/login_screen.dart';
import 'package:auth/registration/registration_screen.dart';
import 'package:auth/route_names.dart';
import 'package:auth/widgets/result_page.dart';
import 'package:flutter/material.dart';

// Since navigation in the app is not complex, there's no need to complicate it using e.g. Bloc.

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

GlobalKey<NavigatorState> get navigationKey => navigatorKey;

BuildContext get dialogContext => navigatorKey.currentState!.overlay!.context;

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case loginPage:
        return MaterialPageRoute(builder: (_) => const LoginScreen());
      case registrationPage:
        return MaterialPageRoute(builder: (_) => const RegistrationScreen());
      case resultPage:
        final args = settings.arguments as ResultScreen;
        return MaterialPageRoute(
            builder: (_) =>
                ResultScreen(text: args.text, assetUrl: args.assetUrl));
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Error'),
        ),
      );
    });
  }
}
