import 'package:flutter/material.dart';

final ColorScheme mainColorTheme = ColorScheme(
  primary: Colors.orange,
  error: Colors.red,
  background: Colors.orange,
  onSurface: Colors.black.withOpacity(0.1),
  onPrimary: Colors.black,
  secondary: Colors.black.withOpacity(0.5),
  surface: Colors.white,
  onError: Colors.white,
  brightness: Brightness.light,
  onSecondary: const Color(0xffAEAFB7),
  onBackground: const Color(0xff3E3EBA),
);

final mainTextStyle = TextStyle(
    color: mainColorTheme.onPrimary, fontSize: 25, fontWeight: FontWeight.w700);

final subTextStyle = TextStyle(color: mainColorTheme.onPrimary, fontSize: 16);

final tipTextStyle = TextStyle(
    color: mainColorTheme.onPrimary, fontSize: 14, fontWeight: FontWeight.w500);

final inputTextStyle = TextStyle(color: mainColorTheme.secondary);

final buttonTextStyle = TextStyle(
  color: mainColorTheme.surface,
);

final mainInputDecoration = InputDecoration(
  labelStyle: inputTextStyle,
  border: InputBorder.none,
  labelText: 'Password',
);
