class AuthData {

  final String username;
  final String encryptedPassword;

  AuthData({
    required this.username,
    required this.encryptedPassword,
  });

  AuthData.fromJson(Map<String, dynamic> json)
      : username = json['username'],
        encryptedPassword = json['encryptedPassword'];

  Map<String, dynamic> toJson() => {
    'username': username,
    'encryptedPassword': encryptedPassword,
  };
}