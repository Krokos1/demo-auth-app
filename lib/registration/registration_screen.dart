import 'package:auth/registration/registration_cubit.dart';
import 'package:auth/registration/registration_state.dart';
import 'package:auth/theme.dart';
import 'package:auth/widgets/default_screen_body_view.dart';
import 'package:auth/widgets/form_button.dart';
import 'package:auth/widgets/form_view.dart';
import 'package:auth/widgets/input_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegistrationScreen extends StatelessWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocProvider(
      create: (_) => RegistrationCubit(),
      child: const RegistrationScreenView());
}

class RegistrationScreenView extends StatelessWidget {
  const RegistrationScreenView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationCubit, RegistrationState>(
      builder: (context, state) {
        final _registrationCubit = context.watch<RegistrationCubit>();
        return Scaffold(
          backgroundColor: mainColorTheme.background,
          appBar: AppBar(
            elevation: 0,
          ),
          body: DefaultScreenBodyView(
            textWidget: Text(
              'Registration',
              style: mainTextStyle,
            ),
            mainWidget: FormView(
              formKey: _registrationCubit.formKey,
              children: [
                InputView(
                  onSaved: _registrationCubit.onUsernameSave,
                  maxLines: 20,
                  labelText: 'Username',
                  validator: _registrationCubit.validateField,
                ),
                InputView(
                  onSaved: _registrationCubit.onPasswordSave,
                  bottomPadding: 50,
                  obscureText: true,
                  labelText: 'Password',
                  validator: _registrationCubit.validateField,
                  maxLines: 20,
                ),
                FormButton(
                  onPressed: _registrationCubit.validate,
                  buttonText: 'Register',
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
