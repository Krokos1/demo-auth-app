import 'dart:convert';

import 'package:auth/auth_info/auth_data.dart';
import 'package:auth/registration/registration_state.dart';
import 'package:auth/router.dart';
import 'package:auth/utils.dart';
import 'package:collection/collection.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegistrationCubit extends Cubit<RegistrationState> {
  final formKey = GlobalKey<FormState>();

  RegistrationCubit() : super(const RegistrationState());

  String? validateField(String? text) {
    if (text != null && text.length < 6) {
      return 'Enter 6 to 20 characters';
    }
    return null;
  }

  void onUsernameSave(String? username) {
    emit(state.changeState(username: username));
  }

  void onPasswordSave(String? password) {
    emit(state.changeState(password: password));
  }


  // SharedPreferences is not the best way for storing data, but login info should be stored on server side anyway :)
  Future<void> validate() async {
    if (formKey.currentState!.validate()) {
      final prefs = await SharedPreferences.getInstance();
      formKey.currentState!.save();
      final storedAuthData = prefs.getStringList('authData');
      final List<AuthData> authData = storedAuthData == null
          ? []
          : storedAuthData
              .map((e) => AuthData.fromJson(json.decode(e)))
              .toList();
      if (authData.firstWhereOrNull(
              (element) => element.username == state.username) !=
          null) {
        showSnackBarText(text: 'Username already exists');
        return;
      }
      List<String> newAuthList = [];
      if (storedAuthData != null) {
        newAuthList = List.from(storedAuthData);
        newAuthList.add(getNewAuthData());
      } else {
        newAuthList = [getNewAuthData()];
      }
      await prefs.setStringList('authData', newAuthList);
      navigatorKey.currentState?.pop();
      showSnackBarText(text: 'Success! Now you can sign in');
    }
  }

  String getNewAuthData() {
    return json.encode(AuthData(
            encryptedPassword:
                sha256.convert(utf8.encode(state.password)).toString(),
            username: state.username)
        .toJson());
  }

  @override
  Future<void> close() {
    return super.close();
  }
}
