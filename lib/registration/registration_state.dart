import 'package:auth/base/base_state.dart';

class RegistrationState extends BaseFormState {
  const RegistrationState({
    String? username,
    String? password,
  }) : super(username: username, password: password);

  RegistrationState changeState({
    String? username,
    String? password,
  }) =>
      RegistrationState(
        username: username ?? this.username,
        password: password ?? this.password,
      );
}
