import 'package:auth/router.dart';
import 'package:auth/theme.dart';
import 'package:flutter/material.dart';

void showSnackBarText({
  required String text,
}) {
  ScaffoldMessenger.of(dialogContext).showSnackBar(SnackBar(
      backgroundColor: mainColorTheme.onPrimary,
      duration: const Duration(seconds: 3),
      content: Text(text)));
}
