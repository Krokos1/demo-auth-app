import 'package:auth/theme.dart';
import 'package:flutter/material.dart';

class InputView extends StatelessWidget {
  final String? Function(String?)? validator;
  final Function(String?) onSaved;
  final int? maxLines;
  final bool obscureText;
  final String labelText;
  final double bottomPadding;

  const InputView(
      {Key? key,
      required this.labelText,
      required this.onSaved,
      this.validator,
      this.maxLines,
      this.obscureText = false,
      this.bottomPadding = 15})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: bottomPadding),
      child: Container(
        decoration: BoxDecoration(
          color: mainColorTheme.onSurface,
          borderRadius: const BorderRadius.all(Radius.circular(30)),
        ),
        child: Padding(
          padding:
              const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
          child: TextFormField(
            validator: validator,
            onSaved: onSaved,
            keyboardType: TextInputType.text,
            maxLength: maxLines,
            obscureText: obscureText,
            decoration: InputDecoration(
              labelStyle: inputTextStyle,
              border: InputBorder.none,
              labelText: labelText,
            ),
          ),
        ),
      ),
    );
  }
}
