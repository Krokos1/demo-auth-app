import 'package:auth/theme.dart';
import 'package:flutter/material.dart';

class FormView extends StatelessWidget {
  final List<Widget> children;
  final GlobalKey<FormState> formKey;

  const FormView({Key? key, required this.children, required this.formKey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: mainColorTheme.surface,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 30),
            child: Form(
              key: formKey,
              child: Column(
                children: children,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
