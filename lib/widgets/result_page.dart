import 'package:auth/theme.dart';
import 'package:auth/widgets/default_screen_body_view.dart';
import 'package:flutter/material.dart';

class ResultScreen extends StatelessWidget {
  final String text;
  final String assetUrl;

  const ResultScreen({Key? key, required this.text, required this.assetUrl})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainColorTheme.background,
      appBar: AppBar(
        elevation: 0,
      ),
      body: DefaultScreenBodyView(
        textWidget: Text(
          text,
          style: mainTextStyle,
        ),
        mainWidget: Expanded(child: Image.asset(assetUrl)),
      ),
    );
  }
}
