import 'package:flutter/material.dart';

class DefaultScreenBodyView extends StatelessWidget {
  final Widget textWidget;
  final Widget mainWidget;

  const DefaultScreenBodyView(
      {Key? key, required this.textWidget, required this.mainWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          textWidget,
          const SizedBox(
            height: 20,
          ),
          mainWidget,
        ],
      ),
    );
  }
}
