import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class BaseFormState extends Equatable {
  final String username;
  final String password;

  const BaseFormState({
    String? username,
    String? password,
  })  : username = username ?? '',
        password = password ?? '';

  @mustCallSuper
  @override
  List<Object?> get props => [username, password];
}
